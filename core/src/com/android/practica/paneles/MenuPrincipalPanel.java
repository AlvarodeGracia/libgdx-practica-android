package com.android.practica.paneles;

import com.android.practica.elementos_pantalla.BotonElemento;
import com.android.practica.elementos_pantalla.ElementoPantalla;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;

public class MenuPrincipalPanel implements Panel {

    private final String TAG = "MenuPrincipalPanel";

    //System,.out.println(TGA+"ajsdkajsdklasjdlasd")

    private ArrayList<ElementoPantalla> elementos;

    public MenuPrincipalPanel(){

        elementos = new ArrayList<ElementoPantalla>();

        InputProcessor inputProcessorOne = new GestureDetector(this);
        Gdx.input.setInputProcessor(inputProcessorOne);

        BotonElemento elemento = new BotonElemento();
        elemento.setX(100);
        elemento.setY(100);
        elemento.setWidth(100);
        elemento.setHeight(100);
        elemento.updateRectangle();
        elementos.add(elemento);

    }


    public void render(SpriteBatch batch, float delta){


        for(ElementoPantalla elemento: elementos){

            elemento.render(batch, delta);

        }

    }

    public void colisiones(){




    }


    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {

        Vector2 aux2 = new Vector2(x, y);
        System.out.println("Elementos "+elementos.size());

        for(ElementoPantalla elemento: elementos){

            System.out.println(aux2.toString());
            if(elemento.getRectangle().contains(aux2))
                System.out.println("Hola me han pulsado");
        }

        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public void pinchStop() {

    }
}
