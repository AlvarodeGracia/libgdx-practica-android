package com.android.practica.paneles;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;

public interface Panel extends GestureDetector.GestureListener {

    public void render(SpriteBatch batch, float delta);
}
