package com.android.practica.elementos_pantalla;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;

public abstract class ElementoPantalla {

    protected float x;
    protected float y;

    protected float width;
    protected float height;

    private ShapeRenderer shapeRenderer;
    static private boolean projectionMatrixSet;

    protected TextureRegion texture;
    protected BitmapFont font;

    protected Rectangle rectangle;

    public ElementoPantalla(){

        shapeRenderer = new ShapeRenderer();
        projectionMatrixSet = false;
        rectangle = new Rectangle();
    }

    public void updateRectangle(){

        System.out.println(rectangle == null);
        rectangle.setX(x);
        rectangle.setY(y);
        rectangle.setWidth(width);
        rectangle.setHeight(height);

    }

    public abstract void render(SpriteBatch batch, float delta);

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public TextureRegion getTexture() {
        return texture;
    }

    public void setTexture(TextureRegion texture) {
        this.texture = texture;
    }

    public BitmapFont getFont() {
        return font;
    }

    public void setFont(BitmapFont font) {
        this.font = font;
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public void setRectangle(Rectangle rectangle) {
        this.rectangle = rectangle;
    }

    public void dibujarFondo(Batch batch) {

        if (texture == null) {

            batch.end();

            if (!projectionMatrixSet) {
                shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
            }

            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

            shapeRenderer.setColor(Color.CYAN);


            shapeRenderer.rect(getX(), getY(), getWidth(), getHeight());

            shapeRenderer.end();
            batch.begin();

        }
    }
}
