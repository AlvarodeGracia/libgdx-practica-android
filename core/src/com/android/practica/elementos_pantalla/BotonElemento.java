package com.android.practica.elementos_pantalla;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class BotonElemento extends ElementoPantalla {

    public BotonElemento(){

        super();

        x = 0;
        y = 0;
        width = 0;
        height = 0;
        texture = null;
        font = null;

    }

    @Override
    public void render(SpriteBatch batch, float delta) {

        dibujarFondo(batch);

    }


}
